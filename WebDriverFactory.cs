﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.IO;

namespace TheTestTask
{
	interface IWebDriverFactory
	{
		IWebDriver Create();
	}

	public class WebDriverFactory : IWebDriverFactory
	{
		public IWebDriver Create()
		{
			var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Drivers");
			var options = new ChromeOptions();
			options.AddArgument("--headless");

			var service = ChromeDriverService.CreateDefaultService(path);
			service.HideCommandPromptWindow = true;

			return new ChromeDriver(service, options);
		}
	}
}