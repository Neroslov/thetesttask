﻿using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using TheTestTask.Models;

namespace TheTestTask
{
	public static class ParserExtensions
	{
		public static void SetupParameters(this IWebDriver driver)
		{
			//Set surename and firstname
			driver.FindElement(By.Id("ct-surename")).ClearElement().SendKeys("Smith");
			driver.FindElement(By.Id("ct-page-as-firstname")).ClearElement();

			//Set checkboxes
			driver.FindElement(By.Id("ct-surname-sim")).SetChecked(true);
			driver.FindElement(By.Id("ct-firstname-sim")).SetChecked(false);
			driver.FindElement(By.Id("ct-find-fighter")).SetChecked(true);
			driver.FindElement(By.Id("ct-find-coach")).SetChecked(true);

			//Set nation
			var selectElement = driver.FindElement(By.Id("ct-nation"));
			new SelectElement(selectElement).SelectByValue("840");

			driver.FindElement(By.Id("submit")).Click();
		}

		private static IWebElement ClearElement(this IWebElement element)
		{
			element.Clear();
			return element;
		}

		private static void SetChecked(this IWebElement element, bool check)
		{
			if (element.Selected != check)
				element.Click();
		}

		public static bool NextPage(this IWebDriver driver)
		{
			var nextPageUrl = driver
				.FindElements(By.XPath("//a[img[@alt='next']]"))
				.FirstOrDefault()
				?.GetAttribute("href");

			if(string.IsNullOrWhiteSpace(nextPageUrl))
				return false;

			driver.Url = nextPageUrl;
			return true;
		}

		public static IEnumerable<string> GetPersonLinks(this IWebDriver driver)
		{
			return driver
				.FindElements(By.CssSelector(".hover tr > td:nth-child(4) a"))
				.Select(el => el.GetAttribute("href"))
				.Distinct();
		}

		public static PersonResult GetPersonResults(this IWebDriver driver, string link)
		{
			driver.Url = link;

			var name = driver.Title
				.Split(':')
				.First()
				.Trim();

			var medalsCount = driver
				.FindElements(By.XPath("//div[h2[contains(.,'Medal count')]]//td[last()]"))
				.Sum(el => int.TryParse(el.Text, out var result) ? result : 0);

			return new PersonResult
			{
				Name = name,
				MedalsCount = medalsCount
			};
		}
	}
}