﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TheTestTask
{
	static class App
	{
		static void Main()
		{
			Console.WriteLine("Start app...");

			var webDriverFactory = new WebDriverFactory();

			using var driver = webDriverFactory.Create();
			driver.Url = "http://www.taekwondodata.com/person_search.html";
			driver.SetupParameters();

			var tasks = new List<Task>();
			Console.WriteLine($"\n{"Name",-20}Medals");
			do
			{
				foreach (var link in driver.GetPersonLinks())
				{
					tasks.Add(Task.Run(() =>
					{
						using var _driver = webDriverFactory.Create();
						var result = _driver.GetPersonResults(link);
						Console.WriteLine($"{result.Name,-20}{result.MedalsCount}");
					}));
				}
			} while (driver.NextPage());

			Task.WaitAll(tasks.ToArray());
			Console.WriteLine("\nEnd app.");
			Console.ReadKey();
		}
	}
}
